﻿using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Helper;

namespace OpenOrderFramework.Data.Repository
{
    public class AmountRepository
    {
        public static AmountEntity GetAmountByClientCode(string code)
        {
            //return new AmountEntity()
            //{
            //    ClientCode = code,
            //    Amount = 50,
            //    Id = 1
            //};

            string sql = @"SELECT TOP 1000 [Id]
                                          ,[ClientCode]
                                          ,[Amount]
                           FROM [MikroMockup].[dbo].[Amounts] WHERE ClientCode = @code";

            return DataHelper.QuerySingle<AmountEntity>(DataBase.MikroDB, sql, new { code });
        }
        public static AmountEntity GetAmountByClientCariCode(string code)
        {


            string sql = @" SELECT c.ct_tutari as CariBalance, c.ct_carikodu as ClientCode, 
                                dbo.fn_CariHesapAnaDovizBakiye('',0, c.ct_carikodu, '', '', NULL, NULL, NULL, 0) as Balance 
                            FROM [MikroDB_V15_BAP18].[dbo].[CARI_HESAP_TEMINATLARI] c WHERE c.ct_Aciklama_no =1 and c.ct_carikodu=@code";

            return DataHelper.QuerySingle<AmountEntity>(DataBase.MikroDB, sql, new { code });
        }
    }
}
