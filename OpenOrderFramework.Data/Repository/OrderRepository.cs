﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Helper;

namespace OpenOrderFramework.Data.Repository
{
    public class OrderRepository
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static bool InsertOrder(OrderEntity order)
        {
            #region sqlInsert
            var sql = @"INSERT INTO [dbo].[SIPARISLER]
                    ([sip_RECid_DBCno]
                    ,[sip_RECid_RECno]
                    ,[sip_SpecRECno]
                    ,[sip_iptal]
                    ,[sip_fileid]
                    ,[sip_hidden]
                    ,[sip_kilitli]
                    ,[sip_degisti]
                    ,[sip_checksum]
                    ,[sip_create_user]
                    ,[sip_create_date]
                    ,[sip_lastup_user]
                    ,[sip_lastup_date]
                    ,[sip_special1]
                    ,[sip_special2]
                    ,[sip_special3]
                    ,[sip_firmano]
                    ,[sip_subeno]
                    ,[sip_tarih]
                    ,[sip_teslim_tarih]
                    ,[sip_tip]
                    ,[sip_cins]
                    ,[sip_evrakno_seri]
                    ,[sip_evrakno_sira]
                    ,[sip_satirno]
                    ,[sip_belgeno]
                    ,[sip_belge_tarih]
                    ,[sip_satici_kod]
                    ,[sip_musteri_kod]
                    ,[sip_stok_kod]
                    ,[sip_b_fiyat]
                    ,[sip_miktar]
                    ,[sip_birim_pntr]
                    ,[sip_teslim_miktar]
                    ,[sip_tutar]
                    ,[sip_iskonto_1]
                    ,[sip_iskonto_2]
                    ,[sip_iskonto_3]
                    ,[sip_iskonto_4]
                    ,[sip_iskonto_5]
                    ,[sip_iskonto_6]
                    ,[sip_masraf_1]
                    ,[sip_masraf_2]
                    ,[sip_masraf_3]
                    ,[sip_masraf_4]
                    ,[sip_vergi_pntr]
                    ,[sip_vergi]
                    ,[sip_masvergi_pntr]
                    ,[sip_masvergi]
                    ,[sip_opno]
                    ,[sip_aciklama]
                    ,[sip_aciklama2]
                    ,[sip_depono]
                    ,[sip_OnaylayanKulNo]
                    ,[sip_vergisiz_fl]
                    ,[sip_kapat_fl]
                    ,[sip_promosyon_fl]
                    ,[sip_cari_sormerk]
                    ,[sip_stok_sormerk]
                    ,[sip_cari_grupno]
                    ,[sip_doviz_cinsi]
                    ,[sip_doviz_kuru]
                    ,[sip_alt_doviz_kuru]
                    ,[sip_adresno]
                    ,[sip_teslimturu]
                    ,[sip_cagrilabilir_fl]
                    ,[sip_prosiprecDbId]
                    ,[sip_prosiprecrecI]
                    ,[sip_iskonto1]
                    ,[sip_iskonto2]
                    ,[sip_iskonto3]
                    ,[sip_iskonto4]
                    ,[sip_iskonto5]
                    ,[sip_iskonto6]
                    ,[sip_masraf1]
                    ,[sip_masraf2]
                    ,[sip_masraf3]
                    ,[sip_masraf4]
                    ,[sip_isk1]
                    ,[sip_isk2]
                    ,[sip_isk3]
                    ,[sip_isk4]
                    ,[sip_isk5]
                    ,[sip_isk6]
                    ,[sip_mas1]
                    ,[sip_mas2]
                    ,[sip_mas3]
                    ,[sip_mas4]
                    ,[sip_Exp_Imp_Kodu]
                    ,[sip_kar_orani]
                    ,[sip_durumu]
                    ,[sip_stalRecId_DBCno]
                    ,[sip_stalRecId_RECno]
                    ,[sip_planlananmiktar]
                    ,[sip_teklifRecId_DBCno]
                    ,[sip_teklifRecId_RECno]
                    ,[sip_parti_kodu]
                    ,[sip_lot_no]
                    ,[sip_projekodu]
                    ,[sip_fiyat_liste_no]
                    ,[sip_Otv_Pntr]
                    ,[sip_Otv_Vergi]
                    ,[sip_otvtutari]
                    ,[sip_OtvVergisiz_Fl]
                    ,[sip_paket_kod]
                    ,[sip_RezRecId_DBCno]
                    ,[sip_RezRecId_RECno]
                    ,[sip_harekettipi]
                    ,[sip_yetkili_recid_dbcno]
                    ,[sip_yetkili_recid_recno]
                    ,[sip_kapatmanedenkod]
                    ,[sip_gecerlilik_tarihi]
                    ,[sip_onodeme_evrak_tip]
                    ,[sip_onodeme_evrak_seri]
                    ,[sip_onodeme_evrak_sira]
                    ,[sip_rezervasyon_miktari]
                    ,[sip_rezerveden_teslim_edilen])
                VALUES
                      (@sip_RECid_DBCno
                      ,(SELECT max(sip_RECno)+1 FROM [MikroDB_V15_BAP18].[dbo].[SIPARISLER] WITH (NOLOCK))
                      ,@sip_SpecRECno
                      ,@sip_iptal
                      ,@sip_fileid
                      ,@sip_hidden
                      ,@sip_kilitli
                      ,@sip_degisti
                      ,@sip_checksum
                      ,@sip_create_user
                      ,@sip_create_date
                      ,@sip_lastup_user
                      ,@sip_lastup_date
                      ,@sip_special1
                      ,@sip_special2
                      ,@sip_special3
                      ,@sip_firmano
                      ,@sip_subeno
                      ,@sip_tarih
                      ,@sip_teslim_tarih
                      ,@sip_tip
                      ,@sip_cins
                      ,@sip_evrakno_seri
                      ,@sip_evrakno_sira
                      ,@sip_satirno
                      ,@sip_belgeno
                      ,@sip_belge_tarih
                      ,@sip_satici_kod
                      ,@sip_musteri_kod
                      ,@sip_stok_kod
                      ,@sip_b_fiyat
                      ,@sip_miktar
                      ,@sip_birim_pntr
                      ,@sip_teslim_miktar
                      ,@sip_tutar
                      ,@sip_iskonto_1
                      ,@sip_iskonto_2
                      ,@sip_iskonto_3
                      ,@sip_iskonto_4
                      ,@sip_iskonto_5
                      ,@sip_iskonto_6
                      ,@sip_masraf_1
                      ,@sip_masraf_2
                      ,@sip_masraf_3
                      ,@sip_masraf_4
                      ,@sip_vergi_pntr
                      ,@sip_vergi
                      ,@sip_masvergi_pntr
                      ,@sip_masvergi
                      ,@sip_opno
                      ,@sip_aciklama
                      ,@sip_aciklama2
                      ,@sip_depono
                      ,@sip_OnaylayanKulNo
                      ,@sip_vergisiz_fl
                      ,@sip_kapat_fl
                      ,@sip_promosyon_fl
                      ,@sip_cari_sormerk
                      ,@sip_stok_sormerk
                      ,@sip_cari_grupno
                      ,@sip_doviz_cinsi
                      ,@sip_doviz_kuru
                      ,@sip_alt_doviz_kuru
                      ,@sip_adresno
                      ,@sip_teslimturu
                      ,@sip_cagrilabilir_fl
                      ,@sip_prosiprecDbId
                      ,@sip_prosiprecrecI
                      ,@sip_iskonto1
                      ,@sip_iskonto2
                      ,@sip_iskonto3
                      ,@sip_iskonto4
                      ,@sip_iskonto5
                      ,@sip_iskonto6
                      ,@sip_masraf1
                      ,@sip_masraf2
                      ,@sip_masraf3
                      ,@sip_masraf4
                      ,@sip_isk1
                      ,@sip_isk2
                      ,@sip_isk3
                      ,@sip_isk4
                      ,@sip_isk5
                      ,@sip_isk6
                      ,@sip_mas1
                      ,@sip_mas2
                      ,@sip_mas3
                      ,@sip_mas4
                      ,@sip_Exp_Imp_Kodu
                      ,@sip_kar_orani
                      ,@sip_durumu
                      ,@sip_stalRecId_DBCno
                      ,@sip_stalRecId_RECno
                      ,@sip_planlananmiktar
                      ,@sip_teklifRecId_DBCno
                      ,@sip_teklifRecId_RECno
                      ,@sip_parti_kodu
                      ,@sip_lot_no
                      ,@sip_projekodu
                      ,@sip_fiyat_liste_no
                      ,@sip_Otv_Pntr
                      ,@sip_Otv_Vergi
                      ,@sip_otvtutari
                      ,@sip_OtvVergisiz_Fl
                      ,@sip_paket_kod
                      ,@sip_RezRecId_DBCno
                      ,@sip_RezRecId_RECno
                      ,@sip_harekettipi
                      ,@sip_yetkili_recid_dbcno
                      ,@sip_yetkili_recid_recno
                      ,@sip_kapatmanedenkod
                      ,@sip_gecerlilik_tarihi
                      ,@sip_onodeme_evrak_tip
                      ,@sip_onodeme_evrak_seri
                      ,@sip_onodeme_evrak_sira
                      ,@sip_rezervasyon_miktari
                      ,@sip_rezerveden_teslim_edilen);  SELECT CAST(SCOPE_IDENTITY() as int)";
            #endregion
            var sqlUpdate = @"UPDATE TOP(1) [dbo].[SIPARISLER] SET sip_RECid_RECno = @Id WHERE sip_RECno = @Id";

            var sqlSelectSipRECno =
                @"SELECT MAX(sip_RECno)+1 FROM [MikroDB_V15_BAP18].[dbo].[SIPARISLER] WITH (NOLOCK)";

            //var sqlSelectEvrakSira = @"SELECT max(sip_evrakno_sira) FROM [MikroDB_V15_BAP18].[dbo].[SIPARISLER] WITH (NOLOCK) where sip_evrakno_seri='I'";
            try
            {
                //var sipRecNo = DataHelper.QuerySingle<int>(DataBase.MikroDB, sqlSelectSipRECno);
              //  var evrakSiraNo = DataHelper.QuerySingle<int>(DataBase.MikroDB, sqlSelectEvrakSira);

              //  order.sip_RECid_RECno = sipRecNo;
              //  order.sip_evrakno_sira = evrakSiraNo;
                order.sip_special1 = "";
                order.sip_special2 = "";
                order.sip_special3 = "";
                order.sip_satici_kod = "";
                order.sip_belgeno = "";
                order.sip_kapatmanedenkod = "";
                order.sip_SpecRECno = 0;


                var inserted = DataHelper.QuerySingle<int>(DataBase.MikroDB, sql, order);

               // DataHelper.Execute(DataBase.MikroDB, sqlUpdate, new { Id = inserted });
                return true;
                
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return false;
            }
        }
        public static int GetSipEvrakNoFromMikro()
        {
            var sql = @"SELECT max(sip_evrakno_sira)+1 FROM [MikroDB_V15_BAP18].[dbo].[SIPARISLER] WITH (NOLOCK) where sip_evrakno_seri='I'";
            
            return DataHelper.QuerySingle<int>(DataBase.MikroDB, sql);
        }
        public static int GetSipEvrakNo(string stockCode, int orderId)
        {
            string sql = @"SELECT TOP 1 sip_evrakno_sira
                           FROM [MikroMockup].[dbo].[Amounts] WHERE ClientCode = @code";
            return 1;
            //return DataHelper.QuerySingle<AmountEntity>(DataBase.MikroDB, sql, new { code });
        }
    }
}
