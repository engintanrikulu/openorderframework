﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Helper;

namespace OpenOrderFramework.Data.Repository
{
    public static class TestRepository
    {
        public static List<TopSoldProductEntity> GetTopSoldProducts(string areaId, string catalogName, int selectCount)
        {
            string sql = @"SELECT TOP {0} P.ProductId, COALESCE(P.cy_list_price,0) as ListPrice, 
			                     (SELECT TOP 1 COALESCE(CurrentPrice,0) FROM Yemeksepeti_marketing.dbo.CalculatedItems (NOLOCK) WHERE CatalogName=P.CatalogName AND ProductId=P.ProductId) 
                                 AS ExtendedPrice,
			                     A.Name AS AreaName,
                                 R.DisplayName as RestaurantDisplayName,
                                 T.CategoryName,
				                 P.IsOpen AS ProductIsOpen,
                                 P.ProductGroup,
                                 P.ProductIsOpen AS ProductIsOpenDescription, 
                                 P.CatalogName, 
				                 P.DefinitionName,
                                 P.DisplayName as ProductDisplayName, 
                                 P.Description,
                                 T.ListingRank as [Rank],
                                 R.IsOpen AS RestaurantIsOpen,
								 R.RestaurantSubState,
                                 RA.MinimumPrice
                            FROM [{1}_tr-TR] P WITH (NOLOCK)
                                INNER JOIN YemekSepeti_analytics.[dbo].[IsBankasi_Maximum_Tektus_Siparis] T (NOLOCK) ON P.ProductId = T.ProductId
                                INNER JOIN Areas A (NOLOCK) ON A.Id = T.RestaurantArea 
                                INNER JOIN RestaurantAreas RA (NOLOCK) ON A.Id = RA.AreaId and T.CategoryName = RA.CategoryName
                                INNER JOIN [{1}_tr-TR] R (NOLOCK) ON R.CategoryName = T.CategoryName
					        WHERE 
                                T.RestaurantArea = @AreaId
                                AND A.Culture = N'tr-TR'
                                AND R.DefinitionName = N'Restaurant'";

            sql = string.Format(sql, selectCount, catalogName);

            string cacheKey =
                $"ProductRepository.GetTopSoldProducts_{areaId}_{selectCount}";
            return DataHelper.QueryListCached<TopSoldProductEntity>(DataBase.MikroDB, sql, new { AreaId = areaId }, cacheKey, DateTime.Now.AddMinutes(10));
        }

        public static void InsertTest(TestItemEntity entity)
        {
            //string sql = @"     declare @ID table (ID int)
            //                    DECLARE @idValue int
            //                    INSERT INTO Items (CatagorieId,Name,Price)
            //                    OUTPUT INSERTED.Id into @ID
            //                    values(@CatagorieId,@Name,@Price)
            //                    select @idValue = ID from @ID
            //                    update Top(1)Items set CatagorieId = @idValue WHERE Id = @idValue";




            string sql = @"    
                                INSERT INTO Items (CatagorieId,Name,Price)
                                OUTPUT INSERTED.Id 
                                values(@CatagorieId,@Name,@Price)";

            //DataHelper.Execute(DataBase.Local, sql, entity);

            var resp=DataHelper.ExecuteScalar<int>(DataBase.Local, sql, entity);


            var sqlUpdate = "Update Top(1) Items Set CatagorieId = @Id WHERE Id=@Id";

            DataHelper.Execute(DataBase.Local, sqlUpdate, new {Id = resp});

            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["LocalDb"].ConnectionString;

                using (var conn = new SqlConnection(connstr))
                {
                    conn.Open();
                    var cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@CatagorieId", SqlDbType.Int).Value = entity.CatagorieId;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value=entity.Name;
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value=entity.Price;


                    cmd.ExecuteNonQuery();

                    conn.Close();
                }

                
                
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

}
