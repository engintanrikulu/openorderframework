﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Helper;

namespace OpenOrderFramework.Data.Repository
{
    public class StockRepository
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static List<ProductEntity> GetStocks()
        {
            log4net.Config.XmlConfigurator.Configure();

            string sql = @"SELECT [msg_S_0088] AS Id
                              ,[CINSI] AS Category
                              ,[STOK KODU] AS StockCode
                              ,[STOK ADI] AS StockName
                              ,[ELDEKI MIKTAR] AS HaveCount
                              ,[SATIS FIYATI] AS ListPrice
                              ,[DOVIZ] AS Currency
                          FROM [MikroDB_V15_BAP18].[dbo].[STOKLAR_CHOOSE_2A] (NOLOCK) where sto_webe_gonderilecek_fl =1";
            //sto_webe_gonderilecek_fl =1

            string cacheKey = "StockRepository.GetStocks";

            try
            {
                return DataHelper.QueryListCached<ProductEntity>(DataBase.MikroDB, sql, null, cacheKey, DateTime.Now.AddMinutes(10));
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                return null;
            }
        }
        public static List<ProductEntity> GetMockStocks()
        {
            string sql = @"SELECT [msg_S_0088] AS Id
                             ,[CINSI] AS Category
                             ,[STOK KODU] AS StockCode
                             ,[STOK ADI] AS StockName
                             ,[ELDEKI MIKTAR] AS HaveCount
                             ,[SATIS FIYATI] AS ListPrice
                             ,[DOVIZ] AS Currency
                         FROM [MikroMockup].[dbo].[Stocks]";

            string cacheKey = "StockRepository.GetStocks";

            return DataHelper.QueryListCached<ProductEntity>(DataBase.MikroDB, sql, null, cacheKey, DateTime.Now.AddMinutes(10));
        }
        public static ProductEntity GetProductByStockId(int id)
        {
            log4net.Config.XmlConfigurator.Configure();
            string sql = @"SELECT [msg_S_0088] AS Id
                             ,[CINSI] AS Category
                             ,[STOK KODU] AS StockCode
                             ,[STOK ADI] AS StockName
                             ,[ELDEKI MIKTAR] AS HaveCount
                             ,[SATIS FIYATI] AS ListPrice
                             ,[DOVIZ] AS Currency
                         FROM [MikroDB_V15_BAP18].[dbo].[STOKLAR_CHOOSE_2A] (NOLOCK)
                         WHERE [msg_S_0088] = @Id";

            try
            {
                return DataHelper.QuerySingle<ProductEntity>(DataBase.MikroDB, sql, new { Id = id });
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }
            return null;
        }
        public static ProductEntity GetMockProductByStockId(int id)
        {
            string sql = @"SELECT [msg_S_0088] AS Id
                             ,[CINSI] AS Category
                             ,[STOK KODU] AS StockCode
                             ,[STOK ADI] AS StockName
                             ,[ELDEKI MIKTAR] AS HaveCount
                             ,[SATIS FIYATI] AS ListPrice
                             ,[DOVIZ] AS Currency
                         FROM [MikroMockup].[dbo].[Stocks] (NOLOCK)
                         WHERE [msg_S_0088] = @Id";

            return DataHelper.QuerySingle<ProductEntity>(DataBase.MikroDB, sql, new { Id = id });
        }
        public static StockEntity GetStockByCode(string code)
        {
            string sql = @"SELECT TOP 1 [sto_RECno]
                                       ,[sto_kod]
	                                   ,[sto_isim]
	                                   ,[sto_toptan_vergi]
                        FROM [MikroDB_V15_BAP18].[dbo].[STOKLAR]
                        WHERE sto_kod = @code";

            return DataHelper.QuerySingle<StockEntity>(DataBase.MikroDB, sql, new { code = code });
        }
    }
}
