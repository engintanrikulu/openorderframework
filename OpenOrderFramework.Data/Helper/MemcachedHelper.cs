﻿using System;
using OpenOrderFramework.Data.Cache;

namespace OpenOrderFramework.Data.Helper
{
    public static class MemcachedHelper
    {
        public static void AddToCache(string key, object value, DateTime expireTime)
        {
            AddToCache(key, value, true, expireTime);
        }

        public static void AddToCache(string key, object value, bool useHybridCache, DateTime expireTime)
        {
            if (string.IsNullOrEmpty(key))
            {
                return;
            }

            DateTime memoryCacheExpireTime = expireTime;

            MemoryCacher.Set(key, value, memoryCacheExpireTime);
        }

        public static void Remove(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return;
            }
            MemoryCacher.Remove(key);
        }

        public static T Get<T>(string key, bool useHybridCaching = true)
        {
            try
            {
                //Cache key'i boşluklarından arındıralım.
                if (string.IsNullOrEmpty(key))
                {
                    return default(T);
                }

                //İlk önce memory üzerinde duran cache'e bakıcaz. Orda varsa direk döncez.
                var cachedItem = MemoryCacher.Get<T>(key);
                if (cachedItem != null)
                {
                    return cachedItem;
                }

                return default(T);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public static CacheEntry GetCacheItem<T>(string key)
        {
            try
            {
                //Cache key'i boşluklarından arındıralım.
                if (string.IsNullOrEmpty(key))
                {
                    return null;
                }

                //İlk önce memory üzerinde duran cache'e bakıcaz. Orda varsa direk döncez.
                var cachedItem = MemoryCacher.GetCacheEntry(key);
                return cachedItem;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void FlushAll()
        {
            MemoryCacher.FlushAll();
        }


        private static DateTime GetMemoryCacheDuration()
        {
            return DateTime.Now.AddMinutes(5);
        }
    }
}
