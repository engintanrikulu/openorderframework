﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace OpenOrderFramework.Data.Helper
{
    public enum DataBase
    {
        MikroDB,
        Local
    }

    public static class DataHelper
    {
        public static IDbConnection GetOpenConnection(DataBase db)
        {
            var connection = new SqlConnection(GetConnectionString(db));
            connection.Open();
            return connection;
        }

        public static void Execute(DataBase db, string sql)
        {
            Execute(db, sql, null);
        }
        public static void Execute(DataBase db, string sql, object param)
        {
            Execute(db, sql, param, null, null, null);
        }
        public static void Execute(DataBase db, string sql, object param, IDbTransaction transaction, int? commandTimeout, CommandType? commandType)
        {
            using (var connection = GetOpenConnection(db))
            {
                connection.Execute(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public static T ExecuteScalar<T>(DataBase db, string sql)
        {
            return ExecuteScalar<T>(db, sql, null);
        }
        public static T ExecuteScalar<T>(DataBase db, string sql, object param)
        {
            return ExecuteScalar<T>(db, sql, param, null, null, null);
        }
        public static T ExecuteScalar<T>(DataBase db, string sql, object param, IDbTransaction transaction, int? commandTimeout, CommandType? commandType)
        {
            using (var connection = GetOpenConnection(db))
            {
                return connection.ExecuteScalar<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }
        public static T ExecuteScalarCached<T>(DataBase db, string sql, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return ExecuteScalarCached<T>(db, sql, null, cacheKey, expireTime, useHybridCaching);
        }

        public static T ExecuteScalarCached<T>(DataBase db, string sql, object param, string cacheKey, DateTime expireTime, CommandType? commandType, bool useHybridCaching = true)
        {
            return ExecuteScalarCached<T>(db, sql, param, null, null, commandType, cacheKey, expireTime, useHybridCaching);
        }
        public static T ExecuteScalarCached<T>(DataBase db, string sql, object param, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return ExecuteScalarCached<T>(db, sql, param, null, null, null, cacheKey, expireTime, useHybridCaching);
        }
        public static T ExecuteScalarCached<T>(DataBase db, string sql, object param, IDbTransaction transaction, int? commandTimeout, CommandType? commandType, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            var cacheEntry = MemcachedHelper.GetCacheItem<T>(cacheKey);
            if (cacheEntry != null)
            {
                return cacheEntry.GetValue<T>();
            }

            var result = ExecuteScalar<T>(db, sql, param, transaction, commandTimeout, commandType);
            if (result != null)
            {
                MemcachedHelper.AddToCache(cacheKey, result, useHybridCaching, expireTime);
            }
            return result;
        }

        public static T QuerySingle<T>(DataBase db, string sql)
        {
            return QuerySingle<T>(db, sql, null);
        }
        public static T QuerySingle<T>(DataBase db, string sql, object param)
        {
            return QuerySingle<T>(db, sql, param, null, true, null, null);
        }
        public static T QuerySingle<T>(DataBase db, string sql, object param, IDbTransaction transaction, bool buffered, int? commandTimeout, CommandType? commandType)
        {
            return QueryList<T>(db, sql, param, transaction, buffered, commandTimeout, commandType).FirstOrDefault();
        }
        public static T QuerySingleCached<T>(DataBase db, string sql, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return QuerySingleCached<T>(db, sql, null, cacheKey, expireTime, useHybridCaching);
        }
        public static T QuerySingleCached<T>(DataBase db, string sql, object param, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return QuerySingleCached<T>(db, sql, param, null, true, null, null, cacheKey, expireTime, useHybridCaching);
        }
        public static T QuerySingleCached<T>(DataBase db, string sql, object param, IDbTransaction transaction, bool buffered, int? commandTimeout, CommandType? commandType, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return QueryListCached<T>(db, sql, param, transaction, buffered, commandTimeout, commandType, cacheKey, expireTime, useHybridCaching).FirstOrDefault();
        }

        public static List<T> QueryList<T>(DataBase db, string sql)
        {
            return QueryList<T>(db, sql, null);
        }
        public static List<T> QueryList<T>(DataBase db, string sql, object param)
        {
            return QueryList<T>(db, sql, param, null, true, null, null);
        }
        public static List<T> QueryList<T>(DataBase db, string sql, object param, IDbTransaction transaction, bool buffered, int? commandTimeout, CommandType? commandType)
        {
            using (var connection = GetOpenConnection(db))
            {
                return connection.Query<T>(sql, param, transaction, buffered, commandTimeout, commandType).ToList();
            }
        }

        public static List<T> QueryListCached<T>(DataBase db, string sql, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return QueryListCached<T>(db, sql, null, cacheKey, expireTime, useHybridCaching);
        }
        public static List<T> QueryListCached<T>(DataBase db, string sql, object param, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            return QueryListCached<T>(db, sql, param, null, true, null, null, cacheKey, expireTime, useHybridCaching);
        }

        public static List<T> QueryListCached<T>(DataBase db, string sql, object param, string cacheKey, DateTime expireTime, CommandType? commandType)
        {
            return QueryListCached<T>(db, sql, param, null, true, null, commandType, cacheKey, expireTime);
        }

        public static List<T> QueryListCached<T>(DataBase db, string sql, object param, IDbTransaction transaction, bool buffered, int? commandTimeout, CommandType? commandType, string cacheKey, DateTime expireTime, bool useHybridCaching = true)
        {
            var entityList = MemcachedHelper.Get<List<T>>(cacheKey, useHybridCaching);
            if (entityList != null)
            {
                return entityList;
            }

            entityList = QueryList<T>(db, sql, param, transaction, buffered, commandTimeout, commandType);

            if (entityList.Any())
            {
                MemcachedHelper.AddToCache(cacheKey, entityList, useHybridCaching, expireTime);
            }

            return entityList;
        }

        private static string GetConnectionString(DataBase database)
        {
            switch (database)
            {
                //Default DB ne olsun karar verilsin.
                case DataBase.MikroDB:
                    return GetConnectionString("MikroDB");
                case DataBase.Local:
                    return GetConnectionString("LocalDB");
                default:
                    throw new ArgumentException("Belirtilen db bulunamadı.");
            }
        }
        private static string GetConnectionString(string databaseName)
        {
            return ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
        }
    }
}
