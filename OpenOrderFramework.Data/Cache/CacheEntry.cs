﻿using System;

namespace OpenOrderFramework.Data.Cache
{
    public class CacheEntry
    {
        //For Reflection 
        public CacheEntry() { }

        public CacheEntry(object value, DateTime expiresAt)
        {
            Value = value;
            ExpiresAt = expiresAt;
        }

        internal DateTime ExpiresAt { get; set; }

        public object Value { get; set; }

        public bool IsEmpty<T>()
        {
            if (Value == null)
            {
                return true;
            }

            if (Value.Equals(default(T)))
            {
                return true;
            }

            return false;
        }

        public T GetValue<T>()
        {
            if (Value == null)
            {
                return default(T);
            }
            if (Value.Equals(default(T)))
            {
                return default(T);
            }

            return (T)Value;
        }
    }
}
