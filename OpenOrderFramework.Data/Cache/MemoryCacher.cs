﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace OpenOrderFramework.Data.Cache
{
    public static class MemoryCacher
    {
        private static ConcurrentDictionary<string, CacheEntry> _memory = new ConcurrentDictionary<string, CacheEntry>();

        private static bool TryGetValue(string key, out CacheEntry entry)
        {
            return _memory.TryGetValue(key, out entry);
        }

        private static void Set(string key, CacheEntry entry)
        {
            _memory[key] = entry;
        }

        /// <summary>
        /// Adds or replaces the value with key. 
        /// </summary>
        private static void CacheSet(string key, object value, DateTime expiresAt)
        {
            CacheEntry entry;
            if (!TryGetValue(key, out entry))
            {
                entry = new CacheEntry(value, expiresAt);
                Set(key, entry);
                return;
            }

            entry.Value = value;
            entry.ExpiresAt = expiresAt;
        }

        public static bool Remove(string key)
        {
            CacheEntry item;
            return _memory.TryRemove(key, out item);
        }

        public static void RemoveAll(IEnumerable<string> keys)
        {
            foreach (var key in keys)
            {
                try
                {
                    Remove(key);
                }
                catch (Exception)
                {
                    //TODO:Log
                }
            }
        }

        public static T Get<T>(string key)
        {
            var value = Get(key);
            if (value != null)
            {
                return (T)value;
            }
            return default(T);
        }

        private static object Get(string key)
        {
            CacheEntry cacheEntry;
            if (_memory.TryGetValue(key, out cacheEntry))
            {
                if (cacheEntry.ExpiresAt < DateTime.UtcNow)
                {
                    _memory.TryRemove(key, out cacheEntry);
                    return null;
                }
                return cacheEntry.Value;
            }
            return null;
        }

        public static CacheEntry GetCacheEntry(string key)
        {
            CacheEntry cacheEntry;
            if (_memory.TryGetValue(key, out cacheEntry))
            {
                if (cacheEntry.ExpiresAt < DateTime.UtcNow)
                {
                    _memory.TryRemove(key, out cacheEntry);
                    return null;
                }
                return cacheEntry;
            }
            return null;
        }

        /// <summary>
        /// Add or replace the value with key to the cache, set to expire at specified DateTime.
        /// </summary>
        /// <remarks>This method examines the DateTimeKind of expiresAt to determine if conversion to
        /// universal time is needed. The version of Set that takes a TimeSpan expiration is faster 
        /// than using this method with a DateTime of Kind other than Utc, and is not affected by 
        /// ambiguous local time during daylight savings/standard time transition.</remarks>
        public static void Set(string key, object value, DateTime expiresAt)
        {
            if (expiresAt.Kind != DateTimeKind.Utc)
            {
                expiresAt = expiresAt.ToUniversalTime();
            }
            CacheSet(key, value, expiresAt);
        }

        public static void FlushAll()
        {
            _memory = new ConcurrentDictionary<string, CacheEntry>();
        }

        public static void RemoveByPattern(string pattern)
        {
            RemoveByRegex(pattern.Replace("*", ".*").Replace("?", ".+"));
        }

        public static void RemoveByRegex(string pattern)
        {
            var regex = new Regex(pattern);
            var enumerator = _memory.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    if (regex.IsMatch(current.Key))
                    {
                        Remove(current.Key);
                    }
                }
            }
            catch (Exception)
            {
                //TODO:Log
            }
        }
    }
}
