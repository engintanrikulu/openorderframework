﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenOrderFramework.Data.Entitiy
{
    public class AmountEntity
    {
        public string ClientCode { get; set; }
        public decimal Balance { get; set; }
        public decimal CariBalance { get; set; }
    }
}
