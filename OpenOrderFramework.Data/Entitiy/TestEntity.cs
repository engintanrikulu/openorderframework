﻿namespace OpenOrderFramework.Data.Entitiy
{
    public class TopSoldProductEntity
    {
        public int Id { get; set; }
        public string CatalogName { get; set; }
        public string CategoryName { get; set; }

        public string ProductId { get; set; }
        public string RestaurantDisplayName { get; set; }
        public string ProductDisplayName { get; set; }
        public string Description { get; set; }
        public string ListPrice { get; set; }
        public string ExtendedPrice { get; set; }
        public string AreaName { get; set; }
        public bool ProductIsOpen { get; set; }
        public string ProductIsOpenDescription { get; set; }
        public bool IsOpen { get; set; }
        public int Rank { get; set; }
        public bool RestaurantIsOpen { get; set; }
        public string RestaurantSubState { get; set; }
        public string MinimumPrice { get; set; }

    }

    public class TestItemEntity
    {
        public int Id { get; set; }
        public int CatagorieId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
