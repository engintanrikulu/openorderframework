﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenOrderFramework.Data.Entitiy
{
    public class ProductEntity
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string StockCode { get; set; }
        public string StockName { get; set; }
        public int HaveCount { get; set; }
        public decimal ListPrice { get; set; }
        public string Currency { get; set; }
        public string Unit { get; set; }
    }
}
