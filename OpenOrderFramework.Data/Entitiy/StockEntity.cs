﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenOrderFramework.Data.Entitiy
{
    public class StockEntity
    {
        public int sto_RECno { get; set; }
        public string sto_kod { get; set; }
        public string sto_isim { get; set; }
        public int sto_toptan_vergi { get; set; }
        public int sto_perakende_vergi { get; set; }
        public int sto_oivvergipntr { get; set; }
    }
}
