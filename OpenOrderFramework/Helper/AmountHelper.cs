﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpenOrderFramework.Data.Repository;
using OpenOrderFramework.Model;
using OpenOrderFramework.Model.Entity;

namespace OpenOrderFramework.Helper
{
    public static class AmountHelper
    {
        public static decimal GetClientAmount(string username)
        {
            var clientAmount = decimal.Zero;

            using (var dbContext = new MikroOrderDbContext())
            {
                try
                {
                    var user = dbContext.Users.FirstOrDefault(f => f.UserName == username);
                    if (user != null)
                    {
                        var userFromMikro = AmountRepository.GetAmountByClientCariCode(user.ClientCode);
                        if (userFromMikro != null)
                        {
                            clientAmount = userFromMikro.CariBalance - userFromMikro.Balance;
                        }
                    }
                }
                catch (Exception ex)
                {
                    clientAmount = 0;
                    throw;
                }
            }

            return clientAmount;
        }
    }
}