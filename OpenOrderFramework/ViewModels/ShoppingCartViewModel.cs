﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OpenOrderFramework.Model.Entity;

namespace OpenOrderFramework.ViewModels
{
    public class ShoppingCartViewModel
    {
        [Key]
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}