﻿namespace OpenOrderFramework.ViewModels
{
    public class ShoppingCartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public int DeleteId { get; set; }
        public decimal ItemTotal { get; set; }
        public int Added { get; set; }
    }
}