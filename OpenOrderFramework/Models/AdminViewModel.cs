﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace OpenOrderFramework.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Müşteri Kodu")]

        public string ClientCode { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
        [Display(Name = "RaporIframe")]
        public string ReportUrl { get; set; }

        [Display(Name = "AnketIframe")]
        public string SurveyUrl { get; set; }
    }
}