﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenOrderFramework.Models
{
    public class ErrorPageDefaults
    {
        public int StatusCode { get; set; }
        public string UserFriendlyMessage { get; set; }
    }
}