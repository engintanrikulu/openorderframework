﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataHandler.Encoder;
using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Repository;
using OpenOrderFramework.Model;
using OpenOrderFramework.Model.Entity;

namespace OpenOrderFramework.Models
{
    public partial class ShoppingCart
    {
        MikroOrderDbContext _dbContext = new MikroOrderDbContext();
        string ShoppingCartId { get; set; }
        public const string CartSessionKey = "CartId";

        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }

        // Helper method to simplify shopping cart calls
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        public int AddToCart(ProductEntity item, string userId, int quantity = 1)
        {
            // Get the matching cart and item instances
            var cartItem = _dbContext.Carts.SingleOrDefault(
                c => c.CartId == ShoppingCartId
                && c.ItemId == item.Id);

            if (cartItem == null)
            {
                // Create a new cart item if no cart item exists
                cartItem = new Cart
                {
                    ItemId = item.Id,
                    CartId = ShoppingCartId,
                    Count = quantity,
                    DateCreated = DateTime.Now,
                    UserId = userId,
                    ListPrice = item.ListPrice,
                    Total = quantity * item.ListPrice,
                    Name = item.StockName,
                    StockCode = item.StockCode
                };
                _dbContext.Carts.Add(cartItem);

                var lineItem = new Item()
                {
                    Name = item.StockName,
                    Price = item.ListPrice,
                    CartId = cartItem.CartId,
                    StockCode = item.StockCode,
                    Quantity = quantity,
                    ItemId = item.Id
                };

                _dbContext.Items.Add(lineItem);
            }
            else
            {
                // If the item does exist in the cart, 
                // then add one to the quantity
                cartItem.Count += quantity;
                cartItem.Total += quantity * cartItem.ListPrice;
            }
            // Save changes
            _dbContext.SaveChanges();

            return cartItem.Count;
        }
        public int UpdateCart(ProductEntity item, int quantity = 1)
        {
            // Get the matching cart and item instances
            var cartItem = _dbContext.Carts.SingleOrDefault(
                c => c.CartId == ShoppingCartId
                && c.ItemId == item.Id);

            if (cartItem != null)
            {
                // If the item does exist in the cart, 
                // then add one to the quantity
                cartItem.Count = quantity;
                cartItem.Total = quantity * cartItem.ListPrice;

                _dbContext.Carts.AddOrUpdate(cartItem);
                _dbContext.SaveChanges();

                return cartItem.Count;
            }
            // Save changes
            return 0;
        }
        public int RemoveFromCart(int id)
        {


            // Get the cart

            var cartItem = _dbContext.Carts.Single(
                cart => cart.CartId == ShoppingCartId
                && cart.Id == id);


            int itemCount = 0;

            if (cartItem != null)
            {
                //if (cartItem.Count > 1)
                //{
                //    cartItem.Count--;
                //    itemCount = cartItem.Count;
                //}
                //else
                //{
                _dbContext.Carts.Remove(cartItem);
                //}
                // Save changes
                _dbContext.SaveChanges();
            }
            return itemCount;
        }

        public void EmptyCart()
        {
            var cartItems = _dbContext.Carts.Where(
                cart => cart.CartId == ShoppingCartId);

            foreach (var cartItem in cartItems)
            {
                _dbContext.Carts.Remove(cartItem);
            }
            // Save changes
            _dbContext.SaveChanges();
        }

        public List<Cart> GetCartItems()
        {
            return _dbContext.Carts.Where(
                cart => cart.CartId == ShoppingCartId).ToList();
        }

        public int GetCount()
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in _dbContext.Carts
                          where cartItems.CartId == ShoppingCartId
                          select (int?)cartItems.Count).Sum();
            // Return 0 if all entries are null
            return count ?? 0;
        }

        public decimal GetItemTotal(int itemId)
        {
            var cartItem = _dbContext.Carts.FirstOrDefault(f => f.CartId == ShoppingCartId && f.ItemId == itemId);

            return cartItem?.Total ?? decimal.Zero;
        }

        public decimal GetTotal()
        {
            // Multiply item price by count of that item to get 
            // the current price for each of those items in the cart
            // sum all item price totals to get the cart total

            var cartItems = _dbContext.Carts.Where(f => f.CartId == ShoppingCartId);

            decimal totalPrice = decimal.Zero;

            foreach (var cart in cartItems)
            {
                var item = StockRepository.GetProductByStockId(cart.ItemId);
                totalPrice += cart.Count * item.ListPrice;
            }

            //decimal? total = (from cartItems in storeDB.Carts
            //                  where cartItems.CartId == ShoppingCartId
            //                  select (int?)cartItems.Count *
            //                  cartItems.Item.Price).Sum();

            return totalPrice;
        }

        public Order CreateOrder(Order order)
        {
            decimal orderTotal = 0;
            order.OrderDetails = new List<OrderDetail>();

            var cartItems = GetCartItems();
            // Iterate over the items in the cart, 
            // adding the order details for each
            foreach (var item in cartItems)
            {
                var stockItem = _dbContext.Items.FirstOrDefault(f => f.CartId == item.CartId && f.ItemId == item.ItemId);
                var orderDetail = new OrderDetail
                {
                    ItemId = item.ItemId,
                    OrderId = order.OrderId,
                    UnitPrice = stockItem.Price,
                    Quantity = item.Count,
                    StockName = item.Name,
                    StockCode = item.StockCode
                };

                // Set the order total of the shopping cart
                orderTotal += (item.Count * stockItem.Price);
                order.OrderDetails.Add(orderDetail);
                _dbContext.OrderDetails.Add(orderDetail);

            }
            // Set the order's total to the orderTotal count
            order.Total = orderTotal;

            // Save the order
            _dbContext.Orders.AddOrUpdate(order);
            _dbContext.SaveChanges();
            // Empty the shopping cart
            EmptyCart();
            // Return the OrderId as the confirmation number
            return order;
        }

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] =
                        context.User.Identity.GetUserId();
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();
                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();
        }

        // When a user has logged in, migrate their shopping cart to
        // be associated with their username
        public void MigrateCart(string userName)
        {
            var shoppingCart = _dbContext.Carts.Where(
                c => c.CartId == ShoppingCartId);

            foreach (Cart item in shoppingCart)
            {
                item.CartId = userName;
            }
            _dbContext.SaveChanges();
        }
    }
}