﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OpenOrderFramework.Configuration;
using OpenOrderFramework.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using OpenOrderFramework.Data.Entitiy;
using OpenOrderFramework.Data.Repository;
using OpenOrderFramework.Model;
using OpenOrderFramework.Model.Entity;
using RestSharp.Authenticators;

namespace OpenOrderFramework.Controllers
{
    [Authorize]
    public class CheckoutController : Controller
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private MikroOrderDbContext _mikroDbContext = new MikroOrderDbContext();
        private AppConfigurations appConfig = new AppConfigurations();

        public List<String> CreditCardTypes
        {
            get { return appConfig.CreditCardType; }
        }

        //
        // GET: /Checkout/AddressAndPayment
        //public ActionResult AddressAndPayment()
        //{
        //    log4net.Config.XmlConfigurator.Configure();
        //    try
        //    {
        //        using (var dbContext = new MikroOrderDbContext())
        //        {
        //            var previousOrder = dbContext.Orders.FirstOrDefault(x => x.Username == User.Identity.Name);

        //            if (previousOrder != null)
        //                return View(previousOrder);
        //            else
        //                return View();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Error(ex.Message, ex);
        //        return View();
        //    }

        //}

        //
        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        public async Task<ActionResult> AddressAndPayment(Order model)
        {
            log4net.Config.XmlConfigurator.Configure();
            //string result =  values[9];

            // TryUpdateModel(order);
            //order.CreditCard = result;

            try
            {

                var currentUserId = User.Identity.GetUserId();
                var manager = new UserManager<User>(new UserStore<User>(new MikroOrderDbContext()));
                var store = new UserStore<User>(new MikroOrderDbContext());
                var currentUser = manager.FindById(User.Identity.GetUserId());
                if ( /*order.SaveInfo &&*/ 1 == 2)
                {

                    //var manager = new UserManager<User>(new UserStore<User>(new MikroOrderDbContext()));
                    //var store = new UserStore<User>(new MikroOrderDbContext());
                    //var ctx = store.Context;
                    //var currentUser = manager.FindById(User.Identity.GetUserId());

                    //currentUser.Address = order.Address;

                    //Save this back
                    //http://stackoverflow.com/questions/20444022/updating-user-data-asp-net-identity
                    //var result = await UserManager.UpdateAsync(currentUser);
                    //await ctx.SaveChangesAsync();

                    //await _mikroDbContext.SaveChangesAsync();
                }

                var order = new Order
                {
                    FirstName = User.Identity.Name,
                    LastName = "Test",
                    Address = "Test",
                    Phone = "Test",
                    City = "Test",
                    Username = User.Identity.Name,
                    Email = User.Identity.Name,
                    OrderDate = DateTime.Now,
                    Status = true
                };


                //Save Order
                _mikroDbContext.Orders.Add(order);
                await _mikroDbContext.SaveChangesAsync();
                //Process the order
                var cart = ShoppingCart.GetCart(this.HttpContext);

                var orderItems = cart.GetCartItems();
                order = cart.CreateOrder(order);

                int index = 0;
                var evrakNo = OrderRepository.GetSipEvrakNoFromMikro();
                foreach (var orderItem in orderItems)
                {
                    var stock = StockRepository.GetStockByCode(orderItem.StockCode);
                  

                    var setOrder = SetOrder(orderItem, stock, index, order.OrderId, currentUser.ClientCode);
                    setOrder.sip_evrakno_sira = evrakNo;
                    var success = OrderRepository.InsertOrder(setOrder);

                    if (!success)
                    {
                        var orderToDelete = _mikroDbContext.Orders.FirstOrDefault(f => f.OrderId == order.OrderId);
                        if (orderToDelete != null)
                        {
                            orderToDelete.Status = false;
                            _mikroDbContext.SaveChanges();
                        }

                        var errorDefault = new ErrorPageDefaults()
                        {
                            UserFriendlyMessage = "Sipariş sırasında bir hata oluştu!"
                        };
                        return View("Error", errorDefault);
                    }

                    index++;
                }


                // CheckoutController.SendOrderMessage(order.FirstName, "New Order: " + order.OrderId,order.ToString(order), appConfig.OrderEmail);

                return RedirectToAction("Complete",
                    new { id = order.OrderId });

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                var errorDefault = new ErrorPageDefaults()
                {
                    UserFriendlyMessage = "Sipariş sırasında bir hata oluştu!"
                };
                return View("Error", errorDefault);
            }
        }

        //
        // GET: /Checkout/Complete
        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            try
            {
                using (var dbContext = new MikroOrderDbContext())
                {
                    bool isValid = dbContext.Orders.Any(o => o.OrderId == id && o.Username == User.Identity.Name);

                    if (isValid)
                    {
                        return View(id);
                    }
                    else
                    {
                        return View("Error");
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        private async Task SendMailToAdmins()
        {
            //   var emailsToSend = string.Join(", ", participantList.Select(a => a.Email));

            var email = new MailMessage()
            {
                To = { "" },
                Body = "<p> Merhabalar, Toplantı tutanağını ekte bulabilirsiniz. </p>",
                IsBodyHtml = false,
                Subject = "meethink"
            };
            using (var smtpClient = new SmtpClient())
            {
                await smtpClient.SendMailAsync(email);
            }
        }

        private static RestResponse SendOrderMessage(String toName, String subject, String body, String destination)
        {
            RestClient client = new RestClient();
            //fix this we have this up top too
            AppConfigurations appConfig = new AppConfigurations();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                    appConfig.EmailApiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                appConfig.DomainForApiKey, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", appConfig.FromName + " <" + appConfig.FromEmail + ">");
            request.AddParameter("to", toName + " <" + destination + ">");
            request.AddParameter("subject", subject);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            IRestResponse executor = client.Execute(request);
            return executor as RestResponse;
        }

        private OrderEntity SetOrder(Cart orderItem, StockEntity stock, int index, int orderId, string clientCode)
        {
            var order = new OrderEntity()
            {
                sip_RECid_DBCno = 0,
                sip_iptal = false,
                sip_fileid = 21,
                sip_hidden = false,
                sip_kilitli = false,
                sip_degisti = false,
                sip_checksum = 0,
                sip_create_user = 10,
                sip_create_date = DateTime.Now,
                sip_lastup_user = 10,
                sip_lastup_date = DateTime.Now,
                sip_firmano = 0,
                sip_subeno = 0,
                sip_tarih = DateTime.Now,
                sip_teslim_tarih = DateTime.Now,
                sip_tip = 0,
                sip_cins = 0,
                sip_evrakno_seri = "I",
                //sip_evrakno_sira = orderId,//"Webden Gelecek",
                sip_satirno = index,//sipariş edilen ürünün o siparişte kaçıncı olduğu (ilk no 0 dan başlıyor.
                sip_belge_tarih = DateTime.Now,
                sip_musteri_kod = clientCode,//siparişi veren kullanıcı kodu (mikrodaki cari_kod)
                sip_stok_kod = orderItem.StockCode,//sipariş verilen ürünn kodu(mikrodaki stok_kod)
                sip_b_fiyat = orderItem.ListPrice,//satış fiyatı(Mikrodaki stok kartına bağlı satış birim fiyatı),
                sip_miktar = orderItem.Count, //sip miktar
                sip_birim_pntr = 1,
                sip_teslim_miktar = 0,
                sip_tutar = orderItem.Total, //birim fiyatı * sipariş miktarı
                sip_iskonto_1 = 0,
                sip_iskonto_2 = 0,
                sip_iskonto_3 = 0,
                sip_iskonto_4 = 0,
                sip_iskonto_5 = 0,
                sip_iskonto_6 = 0,
                sip_masraf_1 = 0,
                sip_masraf_2 = 0,
                sip_masraf_3 = 0,
                sip_masraf_4 = 0,
                sip_vergi_pntr = stock.sto_toptan_vergi, //stoklar tablosundan alınacak vergi puanı,
                sip_vergi = 12, //kdv 
                sip_masvergi = 0,
                sip_opno = 0,
                sip_aciklama = "",
                sip_aciklama2 = "",
                sip_depono = 1,
                sip_OnaylayanKulNo = 0,
                sip_vergisiz_fl = false,
                sip_kapat_fl = false,
                sip_promosyon_fl = false,
                sip_cari_grupno = 0,
                sip_cari_sormerk = "",
                sip_stok_sormerk = "",
                sip_doviz_cinsi = 0,
                sip_doviz_kuru = 1,
                sip_alt_doviz_kuru = 1,
                sip_adresno = 1,
                sip_teslimturu = "",
                sip_cagrilabilir_fl = false,
                sip_prosiprecrecI = 0,
                sip_prosiprecDbId = 0,
                sip_iskonto1 = 0,
                sip_iskonto2 = 1,
                sip_iskonto3 = 1,
                sip_iskonto4 = 1,
                sip_iskonto5 = 1,
                sip_iskonto6 = 1,
                sip_masraf1 = 1,
                sip_masraf2 = 1,
                sip_masraf3 = 1,
                sip_masraf4 = 1,
                sip_isk1 = false,
                sip_isk2 = false,
                sip_isk3 = false,
                sip_isk4 = false,
                sip_isk5 = false,
                sip_isk6 = false,
                sip_mas1 = false,
                sip_mas2 = false,
                sip_mas3 = false,
                sip_mas4 = false,
                sip_kar_orani = 0,
                sip_durumu = 0,
                sip_stalRecId_DBCno = 0,
                sip_stalRecId_RECno = 0,
                sip_planlananmiktar = 0,
                sip_teklifRecId_RECno = 0,
                sip_teklifRecId_DBCno = 0,
                sip_parti_kodu = "",
                sip_lot_no = 0,
                sip_fiyat_liste_no = 1,//kullanıcı kartından alınacak
                sip_Otv_Pntr = 0,
                sip_Otv_Vergi = 0,
                sip_otvtutari = 0,
                sip_OtvVergisiz_Fl = 0,
                sip_RezRecId_DBCno = 0,
                sip_RezRecId_RECno = 0,
                sip_projekodu ="",
                sip_harekettipi = 0,
                sip_yetkili_recid_dbcno = 0,
                sip_yetkili_recid_recno = 0,
                sip_paket_kod = "",
                sip_gecerlilik_tarihi = DateTime.Now.AddDays(365),
                sip_onodeme_evrak_tip = 0,
                sip_onodeme_evrak_seri = "",
                sip_onodeme_evrak_sira = 0,
                sip_rezervasyon_miktari = 0,
                sip_rezerveden_teslim_edilen = 0
            };

            decimal tax = 0;

            if (stock.sto_toptan_vergi == 1)
            {
                tax = (orderItem.Count * orderItem.ListPrice) * 0;
            }
            if (stock.sto_toptan_vergi == 2)
            {
                tax = ((orderItem.Count * orderItem.ListPrice) * 1) / 100;
            }
            if (stock.sto_toptan_vergi == 3)
            {
                tax = ((orderItem.Count * orderItem.ListPrice) * 8) / 100;
            }
            if (stock.sto_toptan_vergi == 4)
            {
                tax = ((orderItem.Count * orderItem.ListPrice) * 18) / 100;
            }
            order.sip_vergi = tax;
            return order;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _mikroDbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}