﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OpenOrderFramework.Model;
using OpenOrderFramework.Model.Entity;
using PagedList.Mvc;
using PagedList;
using OpenOrderFramework.Models;

namespace OpenOrderFramework.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrdersController : Controller
    {
        private MikroOrderDbContext db = new MikroOrderDbContext();

        // GET: Orders
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "price_desc" : "Price";
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date_asc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            using (var dbContext = new MikroOrderDbContext())
            {
                //  IQueryable<Order> orders = new List<Order>();
                IQueryable<Order> orders;
                if (User.IsInRole("Admin"))
                {
                    orders = dbContext.Orders.Where(f => f.Status).AsQueryable().OrderByDescending(f => f.OrderDate);
                }
                else
                {
                    orders = dbContext.Orders.Where(f => f.Username == User.Identity.Name && f.Status).AsQueryable().OrderByDescending(f => f.OrderDate);
                }

             
                if (!String.IsNullOrEmpty(searchString))
                {
                    orders = orders.Where(s => s.FirstName.ToUpper().Contains(searchString.ToUpper())
                                           || s.LastName.ToUpper().Contains(searchString.ToUpper()));
                }
                switch (sortOrder)
                {
                    case "name_desc":
                        orders = orders.OrderByDescending(s => s.FirstName);
                        break;
                    case "Price":
                        orders = orders.OrderBy(s => s.Total);
                        break;
                    case "price_desc":
                        orders = orders.OrderByDescending(s => s.Total);
                        break;
                    case "date_asc":
                        orders = orders.OrderBy(s => s.OrderDate);
                        break;
                    default:  // Name ascending 
                        orders = orders.OrderBy(s => s.FirstName);
                        break;
                }

                int pageSize = 10;
                int pageNumber = (page ?? 1);
                return View(orders.ToPagedList(pageNumber, pageSize));
            }

            //return View(await db.Orders.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            if (User.IsInRole("User"))
            {
                if (order.Username != User.Identity.Name)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            var orderDetails = db.OrderDetails.Where(x => x.OrderId == id);
            order.OrderDetails = await orderDetails.ToListAsync();

            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            db.Orders.Remove(order);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
