﻿using OpenOrderFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using Microsoft.AspNet.Identity;
using OpenOrderFramework.Data.Repository;
using OpenOrderFramework.Helper;
using OpenOrderFramework.Model;
using OpenOrderFramework.ViewModels;

namespace OpenOrderFramework.Controllers
{
    [Authorize]
    public class ShoppingCartController : Controller
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        // GET: /ShoppingCart/
        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };
            var clientAmount = AmountHelper.GetClientAmount(User.Identity.Name);
            ViewBag.amount = clientAmount;
            // Return the view
            return View(viewModel);
        }
        //
        // GET: /Store/AddToCart/5
        [HttpPost]
        public ActionResult AddToCart(int id, int quantity)
        {
            // Retrieve the item from the database

            try
            {
                var addedItem = StockRepository.GetProductByStockId(id);
                var clientAmount = AmountHelper.GetClientAmount(User.Identity.Name);

                var userId = User.Identity.GetUserId();
                // Add it to the shopping cart
                var cart = ShoppingCart.GetCart(this.HttpContext);

                var itemsTotalAfterAdd = cart.GetTotal() + (addedItem.ListPrice * quantity);

                if (itemsTotalAfterAdd > clientAmount)
                {
                    // Display the confirmation message
                    var resultsToReturn = new ShoppingCartRemoveViewModel
                    {
                        Message = " Bu işlem için bakiyeniz yeterli değildir.",
                        CartTotal = cart.GetTotal(),
                        CartCount = cart.GetCount(),
                        DeleteId = id,
                        Added = 0
                    };
                    return Json(resultsToReturn);
                }

                int count = cart.AddToCart(addedItem, userId, quantity);

                // Display the confirmation message
                var results = new ShoppingCartRemoveViewModel
                {
                    Message = Server.HtmlEncode(addedItem.StockName) +
                        " sepetinize eklenmiştir.",
                    CartTotal = cart.GetTotal(),
                    CartCount = cart.GetCount(),
                    ItemCount = count,
                    DeleteId = id,
                    Added = 1
                };
                return Json(results);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);

                var results = new ShoppingCartRemoveViewModel
                {
                    Message = "Ürün sepetinize eklenirken bir hata oluştu!."
                };

                return Json(results);
            }

            // Go back to the main store page for more shopping
            // return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateCart(int id, int quantity)
        {
            try
            {
                var addedItem = StockRepository.GetProductByStockId(id);
                var clientAmount = AmountHelper.GetClientAmount(User.Identity.Name);

                var cart = ShoppingCart.GetCart(this.HttpContext);

                var itemTotal = cart.GetItemTotal(addedItem.Id);

                var itemsTotalAfterAdd = cart.GetTotal() + (addedItem.ListPrice * quantity);

                if (itemsTotalAfterAdd > clientAmount)
                {
                    // Display the confirmation message
                    var resultsToReturn = new ShoppingCartRemoveViewModel
                    {
                        Message = " Bu işlem için bakiyeniz yeterli değildir.",
                        CartTotal = cart.GetTotal(),
                        ItemTotal = itemTotal,
                        CartCount = cart.GetCount(),
                        DeleteId = id,
                        Added = 0
                    };
                    return Json(resultsToReturn);
                }

                int count = cart.UpdateCart(addedItem, quantity);

                // Display the confirmation message
                var results = new ShoppingCartRemoveViewModel
                {
                    Message = Server.HtmlEncode(addedItem.StockName) +
                        " sepetinizde güncellenmiştir.",
                    CartTotal = cart.GetTotal(),
                    CartCount = cart.GetCount(),
                    ItemCount = count,
                    DeleteId = id,
                    ItemTotal = cart.GetItemTotal(id),
                    Added = 1
                };
                return Json(results);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                var results = new ShoppingCartRemoveViewModel
                {
                    Message = "Sepetinizdeki ürünler güncellenirken bir hata oluştu!"
                };
                return Json(results);
            }
        }
        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            try
            {
                using (var dbContext = new MikroOrderDbContext())
                {
                    var cart = ShoppingCart.GetCart(this.HttpContext);
                    var cartId = cart.GetCartId(this.HttpContext);
                    // Get the name of the item to display confirmation

                    // Get the name of the album to display confirmation
                    string itemName = dbContext.Carts
                        .Single(item => item.Id == id && item.CartId == cartId).Name;

                    // Remove from cart
                    int itemCount = cart.RemoveFromCart(id);

                    // Display the confirmation message
                    var results = new ShoppingCartRemoveViewModel
                    {
                        Message = Server.HtmlEncode(itemName) +
                            " sepetinizden çıkartılmıştır.",
                        CartTotal = cart.GetTotal(),
                        CartCount = cart.GetCount(),
                        ItemCount = itemCount,
                        DeleteId = id
                    };
                    return Json(results);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                var results = new ShoppingCartRemoveViewModel
                {
                    Message = "Ürün sepetinizden çıkartılırken bir hata oluştu"
                };
                return Json(results);
            }
        }
        //
        // GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            ViewData["CartCount"] = cart.GetCount();
            return PartialView("CartSummary");
        }
    }
}