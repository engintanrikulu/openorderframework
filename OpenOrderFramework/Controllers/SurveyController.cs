﻿using Microsoft.AspNet.Identity.Owin;
using OpenOrderFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OpenOrderFramework.Controllers
{
    public class SurveyController:Controller
    {
        public SurveyController()
        {

        }
        public SurveyController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public async Task<ActionResult> Index(string username)
        {
            var user = await UserManager.FindByEmailAsync(username);

            var surveyUrl = user.SurveyUrl;

            ViewBag.surveyUrl = surveyUrl;

            return View();
        }
        public async Task<ActionResult> Report(string username)
        {
            var user = await UserManager.FindByEmailAsync(username);

            var reportUrl = user.ReportUrl;

            ViewBag.reportUrl = reportUrl;

            return View();
        }
    }
}