﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using OpenOrderFramework.Data.Repository;
using OpenOrderFramework.Helper;
using OpenOrderFramework.Model;
using PagedList.Mvc;
using PagedList;
using OpenOrderFramework.Models;
using OpenOrderFramework.Model.Entity;

namespace OpenOrderFramework.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private MikroOrderDbContext db = new MikroOrderDbContext();
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Items
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "price_desc" : "Price";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            //var items = StockRepository.GetStocks();
            try
            {
                var items = StockRepository.GetStocks();

                if (items == null || items.Count < 1)
                {
                    var errorDefault=new ErrorPageDefaults()
                    {
                        UserFriendlyMessage = "Güncelleme yapılmaktadır. Kısa bir süre sonra tekrar yayına açılacağız"
                    };
                    return View("Error", errorDefault);
                }

                if (!String.IsNullOrEmpty(searchString))
                {
                    items = items.Where(s => s.StockName.ToUpper().Contains(searchString.ToUpper()) || s.StockCode.ToUpper().Contains(searchString.ToUpper())).ToList();
                }
                switch (sortOrder)
                {
                    case "name_desc":
                        items = items.OrderByDescending(s => s.StockName).ToList();
                        break;
                    case "Price":
                        items = items.OrderBy(s => s.ListPrice).ToList();
                        break;
                    case "price_desc":
                        items = items.OrderByDescending(s => s.ListPrice).ToList();
                        break;
                    default:  // Name ascending 
                        items = items.OrderBy(s => s.StockName).ToList();
                        break;
                }

                var clientAmount = AmountHelper.GetClientAmount(User.Identity.Name);
                ViewBag.amount = clientAmount;

                int pageSize = 10;
                int pageNumber = (page ?? 1);

                return View(items.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
            }

            return View();

            //var items = db.Items.Include(i => i.Catagorie);
            //return View(await items.ToListAsync());
        }

        // GET: Items/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var dbContext = new MikroOrderDbContext())
            {
                Item item = await dbContext.Items.FindAsync(id);
                if (item == null)
                {
                    return HttpNotFound();
                }
                return View(item);
            }

        }

        // GET: Items/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            // ViewBag.CatagorieId = new SelectList(db.Catagories, "ID", "Name");
            return View();
        }

        // POST: Items/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create(Item item)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(item);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //ViewBag.CatagorieId = new SelectList(db.Catagories, "ID", "Name", item.CatagorieId);
            return View(item);
        }

        // GET: Items/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            // ViewBag.CatagorieId = new SelectList(db.Catagories, "ID", "Name", item.CatagorieId);
            return View(item);
        }

        // POST: Items/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //  ViewBag.CatagorieId = new SelectList(db.Catagories, "ID", "Name", item.CatagorieId);
            return View(item);
        }

        // GET: Items/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Item item = await db.Items.FindAsync(id);
            db.Items.Remove(item);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
