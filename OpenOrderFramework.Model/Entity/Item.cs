﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OpenOrderFramework.Model.Entity
{
    public class Item
    {
       // private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();


        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [DisplayName("Catagorie")]
        public int CatagorieId { get; set; }

        [Required(ErrorMessage = "An Item Name is required")]
        [StringLength(160)]
        public string Name { get; set; }

        public int Quantity { get; set; }
        public string StockCode { get; set; }
        public string CartId { get; set; }
        public int ItemId { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [Range(0.01, 999.99,ErrorMessage = "Price must be between 0.01 and 999.99")]
        public decimal Price { get; set; }


    }
}