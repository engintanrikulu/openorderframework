﻿using System.ComponentModel.DataAnnotations;

namespace OpenOrderFramework.Model.Entity
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        public string CartId { get; set; }
        public string UserId { get; set; }
        public int ItemId { get; set; }
        public int Count { get; set; }
        public System.DateTime DateCreated { get; set; }
        public decimal ListPrice { get; set; }
        public decimal Total { get; set; }
        public string Name { get; set; }
        public string StockCode { get; set; }
       
    }
}