﻿namespace OpenOrderFramework.Model.Entity
{
    public class OrderDetail
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public string StockCode { get; set; }
        public string MsgS0088 { get; set; }
        public string StockName { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public bool Status { get; set; }
    }
}
