﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Microsoft.AspNet.Identity.EntityFramework;
using OpenOrderFramework.Model.Common;
using OpenOrderFramework.Model.Entity;

namespace OpenOrderFramework.Model
{
    public partial class MikroOrderDbContext : IdentityDbContext<User>
    {
        public MikroOrderDbContext()
            : base("MikroOrderDbContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<MikroOrderDbContext>());
            this.Configuration.LazyLoadingEnabled = false;
        }

        public static MikroOrderDbContext Create()
        {
            return new MikroOrderDbContext();
        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var user = modelBuilder.Entity<User>().ToTable("Users");
            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithRequired().HasForeignKey(ul => ul.UserId);
            user.Property(u => u.UserName).IsRequired();
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.UserId, r.RoleId }).ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(l => new { l.UserId, l.LoginProvider, l.ProviderKey })
                .ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");

            var role = modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            role.Property(r => r.Name).IsRequired();
            role.HasMany(r => r.Users).WithRequired().HasForeignKey(ur => ur.RoleId);

            modelBuilder.Entity<Order>().Property(x => x.Total).HasPrecision(16, 3);
            modelBuilder.Entity<OrderDetail>().Property(x => x.UnitPrice).HasPrecision(16, 3);
            modelBuilder.Entity<Cart>().Property(x => x.Total).HasPrecision(16, 3);
            modelBuilder.Entity<Cart>().Property(x => x.ListPrice).HasPrecision(16, 3);
            modelBuilder.Entity<Item>().Property(x => x.Price).HasPrecision(16, 3);
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.Now;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }

            return base.SaveChanges();
        }
    }
}